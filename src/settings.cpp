/*
    MIT License

    Copyright (c) 2018 - Faiuolo Valerio

    Permission is hereby granted, free of charge, to any person obtaining a copy
    of this software and associated documentation files (the "Software"), to deal
    in the Software without restriction, including without limitation the rights
    to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
    copies of the Software, and to permit persons to whom the Software is
    furnished to do so, subject to the following conditions:

    The above copyright notice and this permission notice shall be included in all
    copies or substantial portions of the Software.

    THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
    IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
    FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
    AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
    LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
    OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
    SOFTWARE.
*/

#include "settings.hxx"
#include "messages.hxx"

#include <windows.h>
#include <initguid.h>
#include <KnownFolders.h>
#include <ShlObj.h>

#include <boost/filesystem.hpp>
#include <boost/iostreams/device/file_descriptor.hpp>
#include <boost/property_tree/ptree.hpp>
#include <boost/property_tree/ini_parser.hpp>
#include <boost/iostreams/stream.hpp>
#include <boost/exception/all.hpp>
#include <boost/exception/diagnostic_information.hpp> 
#include <boost/exception_ptr.hpp> 
#include <boost/compute/detail/getenv.hpp>


namespace bpt = boost::property_tree;
namespace bio = boost::iostreams;
namespace bfs = boost::filesystem;

using namespace messages;

#ifdef _WIN32
	typedef bpt::wptree boost_property_tree;
#else
	typedef bpt::ptree boost_property_tree;
#endif



settings& settings::I() 
{
	static settings instance;
	return instance;
}

void settings::reloadSettings()
{
	
	std::cout << _TR("Load settings file: %s", _TOC(_settings_path)) << std::endl;
	
	_ini_settings.clear();

	try {
		
		const bfs::path bs_path(_settings_path);
		
		if(bfs::is_regular_file(bs_path))
		{
			boost_property_tree pt;	
			bpt::read_ini(bs_path.string(), pt);
			
			std::string key = "AppName";
			std::wstring current_value = pt.get<okp_string>(INI_APP_KEY);
			_ini_settings.insert(std::pair <std::string, okp_string>(key,current_value));
	
			key = "Lang";
			current_value = pt.get<okp_string>(INI_LANG_KEY, INI_LANG_DEFAULT_VALUE);
			
			if(current_value != L"auto")
				_current_language = _TOC(current_value);
			
			_ini_settings.insert(std::pair <std::string, okp_string>(key,current_value));
		}
		else
		{
			std::string err_msg = _TR("Can't open settings file, try to create a new one in path: %s", _TOC(_settings_path));
			std::string mbox_title = _TR("Error");
			_MBOX(err_msg, mbox_title, _MBOX_ERR);
			writeDefaultSettingsFile();
		}
	}
    catch(boost::exception const&  ex)
    {
		std::string err_msg = _TR("Error processing ini: %s", boost::diagnostic_information(ex));
		std::string mbox_title = _TR("Error");
		_MBOX(err_msg, mbox_title, _MBOX_ERR);
    }
}

okp_string settings::getIniKey(std::string key) const
{
	okp_string settings_ret;
	
	std::map<std::string, okp_string> ini_mirror = _ini_settings;
	std::map<std::string, okp_string>::iterator it;			
	it = ini_mirror.find(key);
			
	if (it != ini_mirror.end())
		settings_ret = it->second;
	
	return settings_ret;
}

void settings::writeDefaultSettingsFile() const
{
	int message_box_res = 0;
	
	try {
		
		const bfs::path bs_path(_settings_path);
		boost_property_tree pt;	
		
		pt.put(INI_APP_KEY, L"");
		pt.put(INI_LANG_KEY, INI_LANG_DEFAULT_VALUE);
		
		bpt::write_ini(bs_path.string(), pt);
		
	}
    catch(boost::exception const&  ex)
    {
		std::string err_msg = _TR("Error writing ini file: %s", boost::diagnostic_information(ex));
		std::string mbox_title = _TR("Error");
		message_box_res = _MBOX(err_msg, mbox_title, _MBOX_ERR);
    }
	
	if(message_box_res == 0)
		_MBOX("Scrittura del file ini completata.", "Completato");
}

void settings::retrieveUserSystemLanguage()
{
	wchar_t buf[100];
    LCID lcid = GetUserDefaultUILanguage();
		
    if (GetLocaleInfo(lcid, LOCALE_SISO639LANGNAME, buf, 100)) 
		_current_language = _TOC(buf);
	else
		_current_language = "en";
}

void settings::retrieveSettingsPath()
{	
	#ifdef _WIN32
		PWSTR path = nullptr;
		HRESULT hr = SHGetKnownFolderPath(FOLDERID_RoamingAppData, 0, nullptr, &path);

		if (SUCCEEDED(hr))
			_settings_path = path;

		CoTaskMemFree(path);
		
		if(!_settings_path.empty())
			_settings_path += 	DIR_SEPARATOR + _TOW(VENDOR_NAME) +
								DIR_SEPARATOR + _TOW(APP_NAME) + 
								DIR_SEPARATOR + _TOW(SETTINGS_FILE) ;
	#endif	
}

settings::settings()
{
	retrieveUserSystemLanguage();
	retrieveSettingsPath();
}