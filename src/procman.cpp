/*
    MIT License

    Copyright (c) 2018 - Faiuolo Valerio

    Permission is hereby granted, free of charge, to any person obtaining a copy
    of this software and associated documentation files (the "Software"), to deal
    in the Software without restriction, including without limitation the rights
    to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
    copies of the Software, and to permit persons to whom the Software is
    furnished to do so, subject to the following conditions:

    The above copyright notice and this permission notice shall be included in all
    copies or substantial portions of the Software.

    THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
    IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
    FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
    AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
    LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
    OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
    SOFTWARE.
*/


#include "procman.hxx"

#include <process.h>
#include <windows.h>
#include <tlhelp32.h>

procman::procman() {}

dword procman::kill_process(okp_string name) const
{
    std::vector<dword> pids = this->get_processes_ids(name);

    if (pids.empty())
        return STATUS_PROCESSES_NOT_FOUND;

    
    dword killed_processes = 0;

    //Kill all processes
    std::vector<dword>::iterator it;
    for (it = pids.begin() ; it != pids.end(); ++it)
    {
        dword current_pid = *it;
        if(terminate_process(current_pid, -1))
			killed_processes++;
    }

    if(killed_processes == 0)
        return STATUS_NOTHING_KILLED;
    else if(killed_processes != pids.size())
        return STATUS_KILL_SOME;
    else
        return STATUS_KILLALL;
}

std::vector<dword> procman::get_processes_ids(okp_string name) const
{
    HANDLE hProcessSnapshot;
    PROCESSENTRY32 pe32;

    std::vector<dword> ids;

    //Create process snapshot
    hProcessSnapshot = CreateToolhelp32Snapshot( TH32CS_SNAPPROCESS, 0 );

    //Invalid process
    if( hProcessSnapshot == INVALID_HANDLE_VALUE )
        return ids;

    //Set struct size
    pe32.dwSize = sizeof( PROCESSENTRY32 );

    // Retrieve information about the first process, and exit if unsuccessful
    if( !Process32First( hProcessSnapshot, &pe32 ) )
    {   
        CloseHandle( hProcessSnapshot );  // clean the snapshot object
        return ids;
    }

    // Iterate in process snapshot
    do
    {  
        // Current process name
        okp_string current_exe = pe32.szExeFile;

        // add current process to vector pid
        if(current_exe == name) 
            ids.push_back(pe32.th32ProcessID);

    } while( Process32Next( hProcessSnapshot, &pe32 ) );

    CloseHandle( hProcessSnapshot );
    return ids;
}

bool procman::terminate_process(dword pid, int exit_code) const
{
    DWORD dwDesiredAccess = PROCESS_TERMINATE;
    BOOL  bInheritHandle  = FALSE;

    HANDLE hProcess = OpenProcess(dwDesiredAccess, bInheritHandle, pid);

    if (hProcess == NULL)
        return false;

    BOOL result = TerminateProcess(hProcess, exit_code);

    CloseHandle(hProcess);

    return result;
}
