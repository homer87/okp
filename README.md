# OKP

OKP is a single process killer, based on local settings file, windows compatible only (for now).

---

## Dependencies

The project depends on the following libraries: 

1. Boost : https://www.boost.org/

---

## Executable

You can find the executables directory: `build/win/x86` for 32 bit windows build, and `build/win/x86_64` for 64bit windows build.

---

## Build WIN32 MSYS2

1. Download and install MSYS2 from: https://www.msys2.org/
2. Enter in MSYS2 general console and run `pacman -Syu` for upgrade all packages
3. In MSYS2 enviroment install:
	* boost for 32-bit enviroment with command `pacman -S mingw-w64-i686-boost` 
	* boost for 64-bit enviroment with command `pacman -S mingw-w64-x86_64-boost` 
	* qt (for qmake) 32-bit enviroment with command `pacman -S mingw-w64-i686-qt5 5.11.2-3 `
	* qt (for qmake) 64-bit enviroment with command `pacman -S mingw-w64-x86_64-qt5 5.11.2-3 `
	* from MSYS enviroment (32bit or 64bit) enter in root directory and run `qmake`
	* run `make -f Makefile.Debug` for debug mode
	* run `make -f Makefile.Release` for release mode


Other build systems (Visual studio for ex.) have not been tested yet.

---

## Configuration

Create `settings.ini` file in `UserProfile\AppData\Roaming\OlosNet\OKP` .

Example settings.ini file:
``` ini

[settings]
AppName=AppName.exe #Process Name
Lang=auto #Language - en (for English), it (for Italian), auto for autoprobe

```

---

## Credits and questions

Software written by Faiuolo Valerio. For questions or information send e-mail to `dev@olosnet.com`

