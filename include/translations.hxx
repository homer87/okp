/*
    MIT License

    Copyright (c) 2018 - Faiuolo Valerio

    Permission is hereby granted, free of charge, to any person obtaining a copy
    of this software and associated documentation files (the "Software"), to deal
    in the Software without restriction, including without limitation the rights
    to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
    copies of the Software, and to permit persons to whom the Software is
    furnished to do so, subject to the following conditions:

    The above copyright notice and this permission notice shall be included in all
    copies or substantial portions of the Software.

    THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
    IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
    FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
    AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
    LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
    OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
    SOFTWARE.
*/

#ifndef TRANSLATIONS_HXX
#define TRANSLATIONS_HXX

#include "defines.hxx"

static const std::map<std::string, std::string> it_translations {
	
        {"Welcome to %s, version %s", "Benvenuto in %s, versione %s"},
        {"This software is released under MIT License\n", "Questo software è rilasciato sotto licenza MIT\n"},
		{"Load settings file: %s", "Carico file di impostazioni: %s"},
		{"Can't open settings file, try to create a new one in path: %s", "Impossibile aprire file di impostazioni, provo a crearne uno nel percorso: %s"},
		{"Error writing ini file: %s", "Errore nella scrittura del file ini: %s"},
		{"Error processing ini: %s", "Errore nel processare il file ini: %s"},
		{"Unable retrieve executable name, set in settings.ini.\n Exit now!", "Impossibile rilevare il nome dell'eseguibile, impostalo nel file settings.ini.\n L'applicazione verrà terminata!"},
		{"Invalid integral argument", "Parametro int non valido"},
		{"Invalid unsigned argument", "Parametro unsigned non valido"},
		{"Invalid float or double argument", "Parametro float o double non valido"},
		{"Invalid string argument", "Parametro stringa non valido"},
		{"Parametro non riconosciuto", "Unrecognized argument"},
		{"Error", "Errore"},
		{"Critical error", "Errore critico"},
		{"Information", "Informazione"},
		{"End", "Terminato"},
		{"Process name: %s\nStatus: all processes killed.", "Nome processo: %s\nStato: tutti i processi terminati."},
		{"Process name: %s\nStatus: some processes killed.", "Nome processo: %s\nStato: alcuni processi terminati."},
		{"Process name: %s\nStatus: no processes killed.","Nome processo: %s\nStato: nessun processo terminato."},
		{"Process name: %s\nStatus: no processes found.","Nome processo: %s\nStato: nessun processo trovato."},
		{"Process name: %s\nStatus: unkown status.", "Nome processo: %s\nStato: stato sconosciuto."}
};

#endif