/*
    MIT License

    Copyright (c) 2018 - Faiuolo Valerio

    Permission is hereby granted, free of charge, to any person obtaining a copy
    of this software and associated documentation files (the "Software"), to deal
    in the Software without restriction, including without limitation the rights
    to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
    copies of the Software, and to permit persons to whom the Software is
    furnished to do so, subject to the following conditions:

    The above copyright notice and this permission notice shall be included in all
    copies or substantial portions of the Software.

    THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
    IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
    FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
    AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
    LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
    OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
    SOFTWARE.
*/

#ifndef MESSAGES_HXX
#define MESSAGES_HXX

#include <typeinfo>

#include <boost/locale.hpp>

#include "defines.hxx"
#include "translations.hxx"
#include "settings.hxx"

#ifdef WIN32
	#include <windows.h>
#endif


/* 
 * Basic printf like and translate functions
 * 
 */

namespace messages
{
	/*
	 * Convert std::string to std::wstring
	 */
	inline std::wstring _TOW(std::string in)
	{
		return boost::locale::conv::utf_to_utf<wchar_t>(in); 
	}
	
	/*
	 * Convert std::wstring to std::string
	 */
	inline std::string _TOC(std::wstring in)
	{
		return boost::locale::conv::utf_to_utf<char>(in);
	}
	
	/*
	 * Basic translate function
	 */
	inline std::string _TR(std::string format)
	{
		std::string current_string = format;
		
		std::string current_language = settings::I().getCurrentLanguage();

		if(current_language == "it")
		{
			std::map<std::string, std::string> current_trans = it_translations;
			std::map<std::string, std::string>::iterator it;
			
			it = current_trans.find(format);
			
			
			if (it != current_trans.end())
				current_string = it->second;
		}
		
		return current_string;
	}
	
    /* 
     * Verify type 
     */ 
    template<typename T>
    inline void _check_type(const char param, T arg)
    {
        switch(param) 
        {
            case 'i' :
            {
                if (!std::is_integral<T>())
                    throw std::invalid_argument(_TR("Invalid integral argument"));
                break;
            }
            case 'd' : 
            {
                if (!std::is_integral<T>())
                    throw std::invalid_argument(_TR("Invalid integral argument"));
                break;
            }
            case 'u' :
            {
                if(!std::is_integral<T>() || std::is_signed<T>())
                    throw std::invalid_argument(_TR("Invalid unsigned argument"));
                break;
            }
            case 'f' :
            {
                if( !std::is_floating_point<T>() )
                    throw std::invalid_argument(_TR("Invalid float or double argument"));
                break;
            }
            case 's' :
            {
                const char *cmp = "";
                std::string strcmp;
                const bool _is_char_s = typeid(cmp) == typeid(T);
                const bool _is_string = typeid(strcmp) == typeid(T);
            
                if(!_is_char_s && !_is_string)
                    throw std::invalid_argument(_TR("Invalid string argument"));
                
                break;
            }
            default:
                throw std::invalid_argument(_TR("Unrecognized argument"));
                break;
        }
    }
    
    /*
     * Standard function 
     */
    inline void _ospr(std::stringstream &buffer, const char* format) // base function
    {
        buffer << format;
    }
 
    /* 
     * Params function (string)
     * Supported flags:
     * d, i : integer
     * u : unsigned
     * f : float or double
     * s : string
    */
    template<typename T, typename... Targs>
    inline void _ospr(std::stringstream &buffer, const char* format, T value, Targs... Fargs)
    {
        for ( ; *format != '\0'; format++ ) {
            
            if ( *format == '%' &&
                *++format != '%') 
            {
                _check_type(*format, value);
                buffer << value;
                _ospr(buffer, format+1, Fargs...); // recursive call
                return;
            }
            buffer << *format;
        }
    }

	/* 
	 * Translate chars with params
	 */
    template<typename... Targs>
    inline std::string _TR(std::string format, Targs... Fargs)
    {
        std::stringstream buffer;
		std::string current_string = _TR(format);
		_ospr(buffer, current_string.c_str(), Fargs...);
		return buffer.str();
    }
	
	#ifdef WIN32
		const unsigned int _MBOX_INFO = MB_OK | MB_ICONINFORMATION ;
		const unsigned int _MBOX_WARN = MB_OK | MB_ICONWARNING ;
		const unsigned int _MBOX_ERR  = MB_OK | MB_ICONERROR ;
		
		inline int _MBOX(std::string text, std::string caption, unsigned int options = _MBOX_INFO, bool in_console = true)
		{			
			std::wstring _converted_caption = _TOW(caption).c_str();
			std::wstring _converted_text = _TOW(text).c_str();
			
			wchar_t* _caption = new wchar_t[_converted_caption.size()];
			wchar_t* _text = new wchar_t(_converted_text.size());
			
			 _caption = const_cast<wchar_t*>(_converted_caption.c_str());
			 _text = const_cast<wchar_t*>(_converted_text.c_str());
															
			const int result =  MessageBoxW( 0, 
											 _text, 
											_caption, 
											options);
			
			if(in_console)
			{
				if(options == _MBOX_ERR)
					std::cerr << text << std::endl;
				else 
					std::cout << text << std::endl;
			}
			
			return result;
		}
	#endif
}


#endif