/*
    MIT License

    Copyright (c) 2018 - Faiuolo Valerio

    Permission is hereby granted, free of charge, to any person obtaining a copy
    of this software and associated documentation files (the "Software"), to deal
    in the Software without restriction, including without limitation the rights
    to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
    copies of the Software, and to permit persons to whom the Software is
    furnished to do so, subject to the following conditions:

    The above copyright notice and this permission notice shall be included in all
    copies or substantial portions of the Software.

    THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
    IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
    FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
    AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
    LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
    OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
    SOFTWARE.
*/

#ifndef PROCMAN_HXX
#define PROCMAN_HXX

#include "defines.hxx"

const dword STATUS_KILLALL = 0;
const dword STATUS_KILL_SOME = 2;
const dword STATUS_NOTHING_KILLED = 3;
const dword STATUS_PROCESSES_NOT_FOUND = 4;

class procman {

    public:
        procman();

        /*
         * Kill all processes with provided name
         */
        dword kill_process(okp_string name) const;
    
    protected:
        /*
         * Get all processes pid 
         */
        std::vector<dword> get_processes_ids(okp_string name) const;

        bool terminate_process(dword pid, int exit_code) const;
};

#endif
