/*
    MIT License

    Copyright (c) 2018 - Faiuolo Valerio

    Permission is hereby granted, free of charge, to any person obtaining a copy
    of this software and associated documentation files (the "Software"), to deal
    in the Software without restriction, including without limitation the rights
    to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
    copies of the Software, and to permit persons to whom the Software is
    furnished to do so, subject to the following conditions:

    The above copyright notice and this permission notice shall be included in all
    copies or substantial portions of the Software.

    THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
    IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
    FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
    AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
    LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
    OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
    SOFTWARE.
*/

#ifndef SETTINGS_HXX
#define SETTINGS_HXX


#include "defines.hxx"

class settings {

    public:
		
		static settings& I();
	
        /*
         * Get settings path
         */
        okp_string getSettingsPath() const {return _settings_path;};

        /*
         * (Re)Load settings
         */
        void reloadSettings();
		
		/*
		 * Get settings key value
		 */
		okp_string getIniKey(std::string key) const;
		
		/*
		 * Write default settings file
		 */
		void writeDefaultSettingsFile() const;
		
		/*
		 * Get current user UI language
		 */
		std::string getCurrentLanguage() const {return _current_language;};
				
	protected:
	
		void retrieveUserSystemLanguage();
		void retrieveSettingsPath();
		
		okp_string _settings_path;
		std::string _current_language;
		std::map<std::string, okp_string> _ini_settings;
		
	private:
		settings();
		settings(const settings&)       = delete;
		void operator= (const settings&) = delete;		
		
};

#endif