/*
    MIT License

    Copyright (c) 2018 - Faiuolo Valerio

    Permission is hereby granted, free of charge, to any person obtaining a copy
    of this software and associated documentation files (the "Software"), to deal
    in the Software without restriction, including without limitation the rights
    to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
    copies of the Software, and to permit persons to whom the Software is
    furnished to do so, subject to the following conditions:

    The above copyright notice and this permission notice shall be included in all
    copies or substantial portions of the Software.

    THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
    IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
    FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
    AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
    LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
    OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
    SOFTWARE.
*/


#ifndef DEFINES_HXX
#define DEFINES_HXX

#include <iostream>
#include <map>
#include <vector>
#include <sstream>


#ifdef _WIN32
	#ifndef WIN32
		#define WIN32
	#endif
#endif

#ifdef WIN32
	typedef std::wstring okp_string;
#else
	typedef std::string okp_string;
#endif

#if (__INTSIZE==2)
	typedef unsigned long dword;
#else
    typedef unsigned int dword;
#endif

// General vars
static const std::string APP_NAME = "OKP";
static const std::string APP_VERSION = "0.1";
static const std::string VENDOR_NAME = "OlosNet";
static const std::string SETTINGS_FILE = "settings.ini";


#ifdef WIN32
	static const std::wstring DIR_SEPARATOR = L"\\";
#else
	static const std::string DIR_SEPARATOR = "/";
#endif


//Ini keys
#ifdef WIN32
	static const std::wstring INI_APP_KEY = L"settings.AppName";
	static const std::wstring INI_LANG_KEY = L"settings.Lang";
	static const std::wstring INI_LANG_DEFAULT_VALUE = L"auto";
#else
	static const std::string INI_APP_KEY = "settings.AppName";
	static const std::string INI_LANG_KEY = "settings.Lang";
	static const std::string INI_LANG_DEFAULT_VALUE = "auto";	
#endif

#endif




