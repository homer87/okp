/*
    MIT License

    Copyright (c) 2018 - Faiuolo Valerio

    Permission is hereby granted, free of charge, to any person obtaining a copy
    of this software and associated documentation files (the "Software"), to deal
    in the Software without restriction, including without limitation the rights
    to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
    copies of the Software, and to permit persons to whom the Software is
    furnished to do so, subject to the following conditions:

    The above copyright notice and this permission notice shall be included in all
    copies or substantial portions of the Software.

    THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
    IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
    FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
    AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
    LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
    OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
    SOFTWARE.
*/

#include <iostream>

#include "defines.hxx"
#include "settings.hxx"
#include "messages.hxx"
#include "procman.hxx"
#include "translations.hxx"

using namespace messages;

#ifdef WIN32
#include <windows.h>
int WINAPI WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, LPSTR lpCmdLine, int nCmdShow)
{
#else
int main(int argc, char *argv[])
{	
#endif

	std::cout << _TR("Welcome to %s, version %s", APP_NAME, APP_VERSION) << std::endl;
	std::cout << "Copyright (c) 2018 - Faiuolo Valerio" << std::endl;
	std::cout << _TR("This software is released under MIT License\n")  << std::endl;
	
	//Load settings
	settings::I().reloadSettings();
	
	
	//Get executable name
	okp_string executable_name = settings::I().getIniKey("AppName");
	if (executable_name.empty())
	{
		std::string message = _TR("Unable retrieve executable name, set in settings.ini.\n Exit now!");
		std::string caption = _TR("Critical error");
		_MBOX(message, caption, _MBOX_ERR);
		
		return -1;		
	}
	
	procman pman;
	//Terminate process
	dword _kill_status = pman.kill_process(executable_name);
	
	//Set status message
	std::string caption = _TR("End");
	std::string message;
	std::string c_executable_name = _TOC(executable_name);
	
	if(_kill_status == STATUS_KILLALL)
		message = _TR("Process name: %s\nStatus: all processes killed.", c_executable_name);
	else if(_kill_status == STATUS_KILL_SOME)
		message = _TR("Process name: %s\nStatus: some processes killed.", c_executable_name);
	else if(_kill_status == STATUS_NOTHING_KILLED)
		message = _TR("Process name: %s\nStatus: no processes killed.", c_executable_name);
	else if(_kill_status == STATUS_PROCESSES_NOT_FOUND)
		message = _TR("Process name: %s\nStatus: no processes found.", c_executable_name);
	else
		message = _TR("Process name: %s\nStatus: unkown status.", c_executable_name);
	
	_MBOX(message, caption);
	
	
    return 0;
}
