TARGET = OKP
CONFIG -= qt
CONFIG += console static

QMAKE_CXXFLAGS += -std=c++1y

INCLUDEPATH += $$PWD/include

SOURCES += \
        main.cpp \
		src\procman.cpp \
		src\settings.cpp 


message("Preparazione dei file di build")
message("Architettura: $$QMAKE_HOST.arch")

CONFIG(debug,debug|release) TYPEDIR = debug
CONFIG(release,debug|release) TYPEDIR = release
message("Tipo di build: $$TYPEDIR")

win32 {
    message("Windows OS")
	QMAKE_CXXFLAGS += -mwin32
    
	DESTDIR = build/win/$$QMAKE_HOST.arch/$$TYPEDIR
    OBJECTS_DIR = build/win/$$QMAKE_HOST.arch/$$TYPEDIR/obj

	LIBS += -lole32 -lboost_iostreams-mt -lboost_system-mt -lboost_filesystem-mt
}

linux-g++ {
    message("Linux OS")

    DESTDIR = build/linux/$$QMAKE_HOST.arch/$$TYPEDIR
    OBJECTS_DIR = build/linux/$$QMAKE_HOST.arch/$$TYPEDIR/obj
}

message("Complete. Now run make")